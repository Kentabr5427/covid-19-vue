Vue.component ('app-car', {
    data: function() {
        return {
            cars:[
                {model:"BMW"},
                {model:"Volvo"},
                {model:"Mercedes-Benz"},
                {model:"Ford"},
                {model:"Audi"},
                {model:"Siat"},
                {model:"Fiat"}
            ]
        }  
    },
    template: '<div><div class="car" v-for="car in cars"><p>{{ car.model }}</p></div></div>'
});
//Локальный компонент (компоненты будут работать только в том сигменте где будут созданы!)
new Vue({
    el:'#app',
    components: {
        'app-car':{
            data: function() {
                return {
                    cars:[
                        {model:"BMW"},
                        {model:"Volvo"},
                        {model:"Mercedes-Benz"},
                        {model:"Ford"},
                        {model:"Audi"},
                        {model:"Siat"},
                        {model:"Fiat"}
                    ]
                }  
            },
            template: '<div><div class="car" v-for="car in cars"><p>{{ car.model }}</p></div></div>'
        }
    }
});
//глобальные компоненты взаимодействуют со всеми компонентами VUE
new Vue({
    el:'#app2',
    data: {}
});